# Maintainer: Bruno Pagani <archange@archlinux.org>
# Contributor: Eli Schwartz <eschwartz@archlinux.org>
# Contributor: Nate Simon <aurpkg (at natesimon.net)>

pkgname=xreader
pkgver=4.2.1
pkgrel=1
pkgdesc="Document viewer for files like PDF and Postscript. X-Apps Project."
arch=(x86_64)
url="https://github.com/linuxmint/${pkgname}"
license=(GPL)
groups=(x-apps)
depends=(poppler-glib webkit2gtk-4.1 xapp)
optdepends=('djvulibre: support for djvu files'
            'libgxps: support for xps files'
            'libspectre: support for dvi and ps files'
            'mathjax2: support for math in epub files'
            'texlive-bin: support for dvi files')
makedepends=(meson samurai intltool itstool gobject-introspection djvulibre
             libgxps libspectre texlive-bin glib2-devel)
source=(${url}/archive/${pkgver}/${pkgname}-${pkgver}.tar.gz)
sha256sums=('33d77a68c21fff6107a0b5fa539073b9ee8e2e82935dd32356458c6870461db6')
b2sums=('f81b68f69c369ff9327fa2eb24d649ff89b1862d6c864bb401cba5d96c2381bb20ee402f5ebdc301c186e2152612f211bf4ca84bce3f599827c58a12a3a388e8')

build() {
  artix-meson ${pkgname}-${pkgver} build \
    --libexecdir=lib/${pkgname} \
    -Dmathjax-directory=/usr/share/mathjax2 \
    -Dcomics=true \
    -Ddjvu=true \
    -Ddvi=true \
    -Dt1lib=true \
    -Dpixbuf=true \
    -Dhelp_files=true \
    -Dintrospection=true
  samu -C build
}

package(){
  DESTDIR="${pkgdir}" samu -C build install
}
